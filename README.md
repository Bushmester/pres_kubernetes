# Kubernetes

Презентация [тут](https://docs.google.com/presentation/d/19kiooGEkh5zE9xxFf5XRkdhTkP8ozA9CQjynaEifLEw/edit#slide=id.g121f08c4e8a_0_122).

Для локальной работы необходимы `minikube` и `kubectl`. Их установка [тут](https://kubernetes.io/docs/tasks/tools/).

Docker файлы в репозитории просто для того, чтобы вы на них посмотрели. Они же милашки, как и ты :3

## Запуск контейнеров в Kubernetes

Создаем локальный кластер

```
minikube start
```

Запускаем наши Pods
```
kubectl apply -f deployment.yaml
```

Запускаем распределитель
```
kubectl apply -f service.yaml
```

Переходим по external-ip, который получим через
```
kubectl get services
```

Обновляем версию образа
```
kubectl set image deployment/cats-deployment cats=bushmester/pres_kubernetes:v2
```

```bushmester/pres_kubernetes:v2``` - образ на [Dockerhub](https://hub.docker.com/repository/docker/bushmester/pres_kubernetes)
