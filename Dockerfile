FROM python:3.8
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY pyproject.toml .
COPY poetry.lock .
RUN pip install poetry && poetry config virtualenvs.create false
RUN poetry install --no-dev

COPY . .

CMD ["gunicorn", "--chdir", "src/", "-k", "uvicorn.workers.UvicornWorker", "asgi:app", "--bind", "0.0.0.0"]
